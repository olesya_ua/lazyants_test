<?php

namespace src\Manufacture\Bakery;

use src\Entity\BaseProduct;
use src\Entity\Ingredient\Coffee;
use src\Entity\Ingredient\Egg;
use src\Entity\Ingredient\Flour;
use src\Entity\Ingredient\Milk;
use src\Entity\Ingredient\Sugar;
use src\Entity\Ingredient\Water;
use src\Entity\Product\Americano;
use src\Entity\Product\Pancake;
use src\Entity\ProductRequest;
use src\Manufacture\ManufactureInterface;

class Bakery implements ManufactureInterface
{
    public function produce(ProductRequest $request): BaseProduct
    {
        $product = $this->create($request->getProductName());
        $this->fillProductOptions($product, $request->getProductOptions());

        if (!$product->isCompleted()) {
            throw new \DomainException('Product is not completed.');
        }

        return $product;
    }

    private function create(string $productName): BaseProduct
    {
        switch ($productName) {
            case BaseProduct::NAME_PANCAKE:
                return $this->createPancake();
            case BaseProduct::NAME_AMERICANO:
                return $this->createAmericano();
            default:
                throw new \DomainException(sprintf('Unknown product %s', $productName));
        }
    }

    private function createPancake(): Pancake
    {
        $pancake = new Pancake();

        $pancake->addIngredient(new Milk())
            ->addIngredient(new Egg())
            ->addIngredient(new Water())
            ->addIngredient(new Flour())
            ->addIngredient(new Sugar());

        return $pancake;
    }

    private function createAmericano(): Americano
    {
        $americano = new Americano();

        $americano->addIngredient(new Coffee())
            ->addIngredient(new Water());

        return $americano;
    }

    private function fillProductOptions(BaseProduct $product, array $options): void
    {
        foreach ($options as $option) {
            $className = sprintf('src\Entity\Ingredient\%s', $option);
            if (class_exists($className)) {
                $product->addIngredient(new $className);
            } else {
                throw new \UnexpectedValueException(sprintf('Unknown ingredient %s', $option));
            }
        }
    }
}
