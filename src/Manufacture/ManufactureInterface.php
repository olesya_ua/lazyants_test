<?php

namespace src\Manufacture;

use src\Entity\ProductRequest;

interface ManufactureInterface
{
    public function produce(ProductRequest $request);
}
