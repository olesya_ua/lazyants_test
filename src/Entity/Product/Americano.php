<?php

namespace src\Entity\Product;

use src\Entity\BaseIngredient;
use src\Entity\BaseProduct;
use src\Entity\Ingredient\Coffee;
use src\Entity\Ingredient\Water;
use src\Entity\Ingredient\Sugar;

class Americano extends BaseProduct
{
    public function getName(): string
    {
        return self::NAME_AMERICANO;
    }

    public function getReceiptIngredients(): array
    {
        return [
            BaseIngredient::NAME_COFFEE => Coffee::class,
            BaseIngredient::NAME_WATER => Water::class,
        ];
    }

    public function getReceiptOptionalIngredients(): array
    {
        return [
            BaseIngredient::NAME_SUGAR => Sugar::class,
        ];
    }
}
