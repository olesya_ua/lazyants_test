<?php

namespace src\Entity;

abstract class BaseProduct
{
    const NAME_PANCAKE = 'pancake';

    const NAME_AMERICANO = 'americano';

    /** @var array|BaseIngredient[] */
    protected $ingredients = [];

    abstract public function getName(): string;

    abstract public function getReceiptIngredients(): array;

    public function getReceiptOptionalIngredients(): array
    {
        return [];
    }

    public function isCompleted(): bool
    {
        if (count($this->getReceiptIngredients()) > count($this->ingredients)) {
            return false;
        }

        foreach ($this->getReceiptIngredients() as $ingredientName => $className) {
            if (!isset($this->getIngredients()[$ingredientName]) || get_class($this->getIngredients()[$ingredientName]) !== $className) {
                return false;
            }
        }

        return true;
    }

    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    public function addIngredient(BaseIngredient $ingredient): BaseProduct
    {
        if (!array_key_exists($ingredient->getName(), $this->getReceiptIngredients())
            && !array_key_exists($ingredient->getName(), $this->getReceiptOptionalIngredients())
        ) {
            throw new \DomainException(
                sprintf('Product %s can not have %s ingredient.', $this->getName(), $ingredient->getName())
            );
        }

        $this->ingredients[$ingredient->getName()] = $ingredient;

        return $this;
    }
}
