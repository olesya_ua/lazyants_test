<?php

namespace src\Entity;

class ProductRequest
{
    /** @var string */
    private $productName;

    /** @var array|string[] */
    private $productOptions = [];

    public function __construct(string $productName, array $productOptions = [])
    {
        $this->productName = $productName;
        $this->productOptions = $productOptions;
    }

    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @return array|string[]
     */
    public function getProductOptions(): array
    {
        return $this->productOptions;
    }
}
