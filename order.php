<?php

use src\Entity\ProductRequest;
use src\Manufacture\Bakery\Bakery;

function myAutoLoad($className)
{
    $classPieces = explode("\\", $className);
    switch ($classPieces[0]) {
        case 'src':
            include __DIR__ .'/'. implode(DIRECTORY_SEPARATOR, $classPieces) . '.php';
            break;
    }
}
spl_autoload_register('myAutoLoad', '', true);

if (isset($argv)) {
    $productOptions = [];

    $optionPrefix = '--with-';
    for ($i = 2; $i < count($argv); $i++) {
        if (($pos = strpos($argv[$i], $optionPrefix)) !== false) {
            $productOptions[] = substr($argv[$i], strlen($optionPrefix));
        }
    }

    $productName = $argv[1];

    try {
        $request = new ProductRequest($productName, $productOptions);
        $bakery = new Bakery();

        $bakery->produce($request);

        printf('%s completed', ucfirst($productName));
    } catch (\Exception $exception) {
        printf('%s was not completed because of %s', ucfirst($productName), $exception->getMessage());
    }
}
