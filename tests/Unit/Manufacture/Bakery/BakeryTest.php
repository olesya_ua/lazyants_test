<?php

namespace tests\Unit\Manufacture\Bakery;

use PHPUnit\Framework\TestCase;
use src\Entity\ProductRequest;
use src\Manufacture\Bakery\Bakery;

class BakeryTest extends TestCase
{
    /**
     * @param string $productName
     * @param array $options
     *
     *
     * @dataProvider producePositiveDataProvider
     */
    public function testProducePositive(string $productName, array $options): void
    {
        $bakery = new Bakery();
        $request = new ProductRequest($productName, $options);
        $product = $bakery->produce($request);

        $this->assertTrue($product->isCompleted());
    }

    public function producePositiveDataProvider(): array
    {
        return [
            ['americano', []],
            ['americano', ['sugar']],
            ['pancake', []]
        ];
    }

    /**
     * @param string $productName
     * @param array $options
     *
     * @dataProvider produceNegativeDataProvider
     */
    public function testProduceNegative(string $productName, array $options): void
    {
        $this->expectException(\Exception::class);

        $bakery = new Bakery();
        $request = new ProductRequest($productName, $options);
        $bakery->produce($request);
    }

    public function produceNegativeDataProvider(): array
    {
        return [
            ['waffle', []],
            ['americano', ['milk']],
            ['pancake', ['jam']]
        ];
    }
}
