<?php

namespace tests\Unit\Entity\Product;

use src\Entity\Product\Americano;
use PHPUnit\Framework\TestCase;

class AmericanoTest extends TestCase
{
    public function testIngredients()
    {
        $americano = new Americano();

        $this->assertEquals('americano', $americano->getName());

        $this->assertArrayHasKey('coffee', $americano->getReceiptIngredients());
        $this->assertArrayHasKey('water', $americano->getReceiptIngredients());
        $this->assertArrayNotHasKey('sugar', $americano->getReceiptIngredients());

        $this->assertArrayHasKey('sugar', $americano->getReceiptOptionalIngredients());
        $this->assertArrayNotHasKey('milk', $americano->getReceiptOptionalIngredients());
    }
}
