<?php

namespace App\Tests\Unit\Entity\Product;

use PHPUnit\Framework\TestCase;
use src\Entity\Product\Pancake;

class PancakeTest extends TestCase
{
    public function testIngredients()
    {
        $pancake = new Pancake();

        $this->assertEquals('pancake', $pancake->getName());

        $this->assertArrayHasKey('milk', $pancake->getReceiptIngredients());
        $this->assertArrayHasKey('egg', $pancake->getReceiptIngredients());
        $this->assertArrayHasKey('water', $pancake->getReceiptIngredients());
        $this->assertArrayHasKey('flour', $pancake->getReceiptIngredients());
        $this->assertArrayHasKey('sugar', $pancake->getReceiptIngredients());
        $this->assertArrayNotHasKey('salt', $pancake->getReceiptIngredients());

        $this->assertArrayNotHasKey('chocolate', $pancake->getReceiptOptionalIngredients());
        $this->assertArrayNotHasKey('jam', $pancake->getReceiptOptionalIngredients());
    }
}
